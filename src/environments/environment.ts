// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyClq8DxwladYyVJoh62jmbmj9uBYTdxR3w",
  authDomain: "books-cec21.firebaseapp.com",
  databaseURL: "https://books-cec21.firebaseio.com",
  projectId: "books-cec21",
  storageBucket: "books-cec21.appspot.com",
  messagingSenderId: "388448979534",
  appId: "1:388448979534:web:06fdaa6fe11a3f1e34511f",
  measurementId: "G-KGN1FMCKR9"
  }  
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
