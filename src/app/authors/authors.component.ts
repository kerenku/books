import { Component, OnInit } from '@angular/core';
import {AuthorsService} from './../authors.service';
import {Observable} from 'rxjs';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-authors',
  templateUrl: './authors.component.html',
  styleUrls: ['./authors.component.css']
})


export class AuthorsComponent implements OnInit {

  constructor(private route: ActivatedRoute,private authorsservice:AuthorsService) { }

  author; 
  id;
  newauthor:string;
  authors:any;
  authors$:Observable<any>;
  
  add(){
    this.authorsservice.addAuthors(this.newauthor);
  }
  
  // typesOfShoes: object[] =  [{id:1, author:'Thomas Mann'},{id:2, author:'Leo Tolstoy'},{id:3, author:'Lewis Carrol'}]

  ngOnInit() {
    // this.authors = this.authorsservice.getAuthors().subscribe(
    //   (authors) => this.authors = authors
    this.authors$ = this.authorsservice.getAuthors();
    this.author = this.route.snapshot.params.author;
    this.id = this.route.snapshot.params.id;
  }


}