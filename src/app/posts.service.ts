import { Post } from './interfaces/post';
import { User } from './interfaces/user';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class PostsService {

  apiurl = "https://jsonplaceholder.typicode.com/posts "
  apiurluser = "https://jsonplaceholder.typicode.com/users"


  constructor(private _http: HttpClient) { }

  getPost(){
    return this._http.get<Post[]>(this.apiurl);
  }
  
  getUser(){
    return this._http.get<User[]>(this.apiurluser);
  }
}

