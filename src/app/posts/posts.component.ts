import { PostsService } from './../posts.service';
import { Post } from '../interfaces/post';
import { Component, OnInit } from '@angular/core';
import { User } from '../interfaces/user';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {

  panelOpenState = false;
 

  Posts$: Post[];
  User$: User[];

  constructor(private postsrvice: PostsService) { }

  ngOnInit() {
    return this.postsrvice.getPost()
    .subscribe(data =>this.Posts$ = data ); 
    this.postsrvice.getUser()
    .subscribe(data =>this.User$ = data );
  }

}